const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const path = require('path');
const precss = require('precss');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'production',
    //devtool: 'eval',
    entry: [
        './src/index.js'
    ],
    
    plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({filename:'./main.css', disable:false}),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      tether: 'tether',
      Tether: 'tether',
      'window.Tether': 'tether',
      Popper: ['popper.js', 'default'],
      'window.Tether': 'tether',
      Alert: 'exports-loader?Alert!bootstrap/js/dist/alert',
      Button: 'exports-loader?Button!bootstrap/js/dist/button',
      Carousel: 'exports-loader?Carousel!bootstrap/js/dist/carousel',
      Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
      Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
      Modal: 'exports-loader?Modal!bootstrap/js/dist/modal',
      Popover: 'exports-loader?Popover!bootstrap/js/dist/popover',
      Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
      Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
      Tooltip: "exports-loader?Tooltip!bootstrap/js/dist/tooltip",
      Util: 'exports-loader?Util!bootstrap/js/dist/util'
    })
    
    ],
    
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
    
      module: {
    // configuration regarding modules

    rules: [
        
        { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' },
        
        {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader', // transpile to ES5
          options: {
            presets: ['es2015']
          }
        }]
        },
              
        {
            test: /\.css$/, use: ['style-loader', 'css-loader', 'postcss-loader']
        },
        
        {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
            use:[

            {
                loader: 'css-loader', // translates CSS into CommonJS modules
            }, 
            {
                loader: 'postcss-loader', // Run post css actions
                options: {
                    plugins: function () { // post css plugins, can be exported to postcss.config.js
                        return [
                            require('precss'),
                            require('autoprefixer')
                        ];
                    }
                }
            }, 
            {
                loader: 'sass-loader' // compiles Sass to CSS
            }
            ]
        })
        },
        
       // Bootstrap 4
      {
        test: /bootstrap\/dist\/js\/umd\//, use: 'imports-loader?jQuery=jquery'
      }
        ]
    
}
    
}