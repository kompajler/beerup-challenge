import 'bootstrap';
import'./main.scss';

import popupBeerInfo from './popupBeerInfo.js';
import * as service from './service.js';

var app=app || {
    items: null
};

app.init=function(){
	console.log("app.init");
	
    var _this=this;
    
    popupBeerInfo.init();
    
	var $beerContainer=$(".beer-items-inner-container");
	
	var r=$.when(service.loadBeerList(1,20));
	r.done(function(items){
		_this.items=items;
        
		//Load the list of beers
		for(var i=0; i<items.length; i++){			
			var $item=$beerContainer.find(".beer-item").eq(i);
			if($item.length>0){
				app.fillBeerCardInfo($item, items[i]);
				$item.find(".wireframe").fadeOut();
			}
		}
		
		//init events for the beer cards
		app.initClickEvents();
		app.initDragDropEvents();
	});
	
}

//Init beer cards click event
app.initClickEvents=function(){
	console.log("app.initClickEvents");

	var _this=this;
	
	$(".beer-item").off("click").on("click", function(){
        var $item=$(this);
        var i=$item.data("index");
        if(i!=undefined){
            var options={
                setFavoriteState: function(state){
                    app.setFavoriteState($item, state);
                },
                
                addToCrate: function(){
                    
                }
            }
            popupBeerInfo.show(_this.items[i], options); 
        }
           
    });
};

app.setFavoriteState=function($item, state){
    console.log("app.setFavoriteState");
    
    alert($item.find(".favorite-indicator").length);
    
    if(state==true){
        $item.find(".favorite-indicator").addClass("is-favorite");
    }else{
        $item.find(".favorite-indicator").removeClass("is-favorite");
    }
}

//Init drag & drop events for beer cards
app.initDragDropEvents=function(){
	console.log("app.initDragDropEvents");
	
	//Drag start
    $(".beer-item").off("dragstart").on("dragstart", function(e){
		console.log("dragstart");
		
		var img = document.createElement("img");
		img.src = "dist/img/beer-wireframe.png";
		
		e.originalEvent.dataTransfer.setDragImage(img, 0, 0);
		console.log(JSON.stringify(e));
	});
	
	//event for prevention of dropping a beer on an occupied space inside beer crate
	$(".beer-socket").off("dragover").on("dragover",function(e){
		if(e.target.className.indexOf("full")>-1){
		}else{
			e.preventDefault();
		}
	});
	
	//Drop event
	$(".beer-socket").off("drop").on("drop",function(e){
		console.log("drop");
		
		e.target.className="beer-socket full";
	});
};

//fill beer information shown on a beer card
app.fillBeerCardInfo=function($card, item){
	console.log("app.fillBeerCardInfo");
	
	var name=item.name;
	if(name!=null && name!=undefined && name.length>15) name=name.substring(0,15)+"...";
	
	if($card!=null && $card!=undefined){
        
        //favorite state
        var isFavorite=service.getFavoriteState(item.id);
        if(isFavorite){
            $card.find(".favorite-indicator").addClass("is-favorite");
        }else{
            $card.find(".favorite-indicator").removeClass("is-favorite");
        }
        
        //beer info
		$card.find("img").attr("src",item.image_url);
		$card.find(".name").html(name);
		$card.find(".ibu .value").html(item.ibu);
		$card.find(".abv .value").html(item.abv+"%");
	}
}

//
$("document").ready(function(){
	app.init();
});