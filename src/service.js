
var endPoint = "https://api.punkapi.com/v2";


//toggle favorite state for a given beer item id
function toggleFavoriteState(id){
	console.log("toggleFavoriteState");
	
	//Define a promise
    var deferred = $.Deferred();
	
	setTimeout(function(){
		var favorites=[];
		
		var favoritesItem=window.localStorage.getItem("favorites");
		if(favoritesItem!=null){
			favorites=JSON.parse(favoritesItem);
		}
		var newState=false;
        
		var index=favorites.indexOf(id);
		if(index>-1){
			//remove from favorites
			favorites.splice(index,1);
		}else{
			//add to favorites
			favorites.push(id);
            newState=true;
		}
		
		window.localStorage.setItem("favorites", JSON.stringify(favorites));
		deferred.resolve(newState);
	}, 1300);
	
	return deferred.promise();
}

function getFavoriteState(id){
	console.log("getFavoriteState "+id);
	
	var favorites=[];
	
	var favoritesItem=window.localStorage.getItem("favorites");
	if(favoritesItem!=null){
		favorites=JSON.parse(favoritesItem);
	}
	
	return favorites.indexOf(id)>-1;
}

function loadBeerList(pageNumber, itemsPerPage){
	console.log("loadBeerList");
	
	//Define a promise
    var deferred = $.Deferred();

	var accessPoint = endPoint+"/beers?page="+pageNumber+"&per_page="+itemsPerPage;
	$.ajax({
		url: accessPoint,
		type: 'GET',
		contentType: "application/json; charset-utf-8",
		beforeSend: function(xhr) {

		},
		success: function(response){
			deferred.resolve(response);	
		},
		fail: function(){
			alert("Nismo se u mogućnosti povezati na sustav. Molimo pokušajte kasnije");
			deferred.reject();
		},
		error: function(result){
			alert("Nismo se u mogućnosti povezati na sustav. Molimo pokušajte kasnije");
			deferred.reject();			
		},
		complete:function(e){
		}
	});
	
	return deferred.promise();
}

module.exports={
    toggleFavoriteState: toggleFavoriteState,
	getFavoriteState: getFavoriteState,
    loadBeerList: loadBeerList
};