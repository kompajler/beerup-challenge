import * as service from './service.js';

//start of the module
export default {
    $modal: null,
    btnToggleFavoriteState: null,

    init: function(){
        console.log("popupBeerInfo.init");

        var _this=this;
        
        this.$modal=$('.popup-beer-info');
        this.btnToggleFavoriteState=this.$modal.find(".btn-toggle-favorites");
        this.initEvents();
    },

    show: function(beerItem, options){
        console.log("popupBeerInfo.show");

        this.id=beerItem.id;
        this.options=options;

        //favorite state
        var state=service.getFavoriteState(beerItem.id);
        popupBeerInfo.setFavoriteState(state);


        //image
        this.$modal.find(".beer-img").attr("src", beerItem.image_url);

        //name
        this.$modal.find(".lbl-name").html(beerItem.name);

        //ibu
        this.$modal.find(".info-ibu .value").html(beerItem.ibu);

        //abv
        this.$modal.find(".info-abv .value").html(beerItem.abv+"%");

        //description
        this.$modal.find(".description").html(beerItem.description);


        this.$modal.modal('show'); 
    },

    initEvents: function(){
        console.log("popupBeerInfo.initEvents");  
        
        //check if selected beer is in a list of favorite beers
        this.btnToggleFavoriteState.off("click").on("click", function(){
            var r=$.when(service.toggleFavoriteState(this.id));
            r.done(function(result){
                _this.options.setFavoriteState(result);
                _this.setFavoriteState(result);
            })
        });

    },
    
    setFavoriteState: function(state){
        console.log("popupBeerInfo.setFavoriteState");

        if(state==true){
            this.$modal.find(".btn-toggle-favorites").addClass("is-favorite");
        }else{
            this.$modal.find(".btn-toggle-favorites").removeClass("is-favorite");
        }
    }
}